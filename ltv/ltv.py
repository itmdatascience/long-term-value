import re
import pandas as pd
import numpy as np
from sqlalchemy import create_engine
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import gamma
from opdap.opdap import OpdapRegressor, VariableFlag
from opdap.features import Feature, FeatureList
from pandas.testing import assert_index_equal


def intify(X):
    try:
        return int(X)
    except ValueError:
        return None


def split_train_from_test(idxs, fraction=0.1):
    no_in_test = int(len(idxs) * fraction)
    idx_test = np.random.choice(idxs, no_in_test, replace=False)
    return np.array(list(set(idxs) - set(idx_test))), idx_test


def cdf_gamma(x, a, b, c):
    return a * gamma.cdf(x / b, c)


class DataManager:
    def __init__(self, index_columns=['date', 'level1', 'level2'], cut_columns=None):
        self._df_raw = None
        self._df_revenues = None
        self._df_full = None
        self._time_columns = None
        self._net_rev_columns = []
        self._general_trainings_columns = ['spend', 'clicks', 'installs', 'impressions']
        self._optional_columns = ['purchases', 'purchasers']
        self._index_columns = index_columns
        self._cut_columns = cut_columns

    @property
    def df_raw(self):
        if self._df_raw is None:
            self._df_raw = self._read_main_table()
        return self._df_raw

    @property
    def time_columns(self):
        if self._time_columns is None:
            self._time_columns = self._build_time_column_dict()
            self._set_origin_day()
        return self._time_columns

    @property
    def df_revenues(self):
        if self._df_revenues is None:
            self._df_revenues = self._build_monotonous_frame()
        return self._df_revenues

    @property
    def df_full(self):
        if self._df_full is None:
            self._df_full = self._build_full_frame()
        return self._df_full

    @property
    def index_columns(self):
        return self._index_columns

    @index_columns.setter
    def index_columns(self, val):
        self._index_columns = val

    def init_database(self, user, pw, table='portfolio_analytics.ua_ml_fb_zombiegunship'):
        self._conn = create_engine(f'postgresql://{user}:{pw}@flaregames-etl.bi.flarecloud.net:5439/flaregames_etl_live')
        self._table = table

    def _read_main_table(self):
        df = pd.read_sql(f'select * from {self._table};',
                         con=self._conn, parse_dates='date').replace({0: np.nan})
        df.loc[:, 'iOS'] = df.level1.str.contains('iOS').astype(int)
        df.loc[:, 'Android'] = df.level1.str.contains('Android').astype(int)
        if self._cut_columns is not None:
            for k, v in self._cut_columns.items():
                df = df[df[k] == v]

        return df[~df.installs.isnull()]

    def _build_time_column_dict(self):
        time_column_dict = {}
        for c in self.df_raw.columns.values:
            parts = c.rsplit('_', 1)
            if len(parts) == 1:
                continue
            if 'd' in parts[-1]:
                if parts[0] not in time_column_dict.keys():
                    time_column_dict[parts[0]] = []
                try:
                    time_column_dict[parts[0]].append(int(parts[-1].strip('d')))
                except ValueError:
                    print(parts)
        #try:
        #    del time_column_dict['purchasers']
        #except IndexError:
        #    pass
        return time_column_dict

    def _build_monotonous_frame(self):
        df_ = self.df_raw[[f'net_revenue_d{d}' for d in self.time_columns['net_revenue']]].T

        df_unstacked = df_.unstack().rename('net_revenue')
        df_unstacked.index.names = ['row', 'revenue_day']
        df_unstacked = df_unstacked.reset_index()
        df_unstacked.loc[:, 'max_id'] = df_unstacked.groupby('row')['net_revenue'].transform('idxmax')
        df_unstacked.loc[:, 'max_rev'] = df_unstacked.groupby('row')['net_revenue'].transform('max')
        df_unstacked.loc[(df_unstacked.index > df_unstacked.max_id) &
                         ~df_unstacked.net_revenue.isnull(), 'net_revenue'] = df_unstacked.max_rev

        return df_unstacked.pivot(index='row', columns='revenue_day', values='net_revenue')[[
            f'net_revenue_d{d}' for d in self.time_columns['net_revenue']]]

    def _set_origin_day(self):
        for cat_key, days in self.time_columns.items():
            self.df_raw.rename(columns={f'{cat_key}_d{d}': f'{cat_key}_d{d+1}' for d in days}, inplace=True)
            self.df_raw.loc[:, f'{cat_key}_d0'] = 0
        for cat_key in self.time_columns.keys():
            self.time_columns[cat_key] = [0] + [d+1 for d in self.time_columns[cat_key]]
        self._net_rev_columns = [f'net_revenue_d{d}' for d in self.time_columns['net_revenue']]

    def _build_full_frame(self):
        df = pd.concat([self.df_revenues, self.df_raw[self._general_trainings_columns + self._index_columns +
                                                      self.get_optional_columns()]], axis=1)
        df.fillna(0, inplace=True)
        df = pd.concat([df, self.df_raw['date'].dt.weekday.rename('weekday')], axis=1)
        df = pd.concat([df, self.df_raw[['iOS', 'Android']]], axis=1)
        return df

    def get_dreisatz(self, day, idx_train=None, idx_test=None):
        df_ = self.df_full.copy() if idx_train is None else self.df_full.loc[idx_train]
        m1 = df_[f'net_revenue_d{day}'].mean()
        mean_all = df_[[f'net_revenue_d{d}' for d in self.time_columns['net_revenue']]].mean()

        if idx_test is not None:
            df = pd.DataFrame(np.tile((self.df_full.loc[idx_test][f'net_revenue_d{day}'] / m1),
                                      (len(self.time_columns['net_revenue']), 1)).T *
                              np.tile(mean_all, (len(idx_test), 1))).set_index(idx_test)
        elif idx_train is not None:
            df = pd.DataFrame(np.tile((self.df_full.loc[idx_train][f'net_revenue_d{day}'] / m1),
                                      (len(self.time_columns['net_revenue']), 1)).T *
                              np.tile(mean_all, (len(idx_train), 1))).set_index(idx_train)
        else:
            df = pd.DataFrame(np.tile((self.df_full[f'net_revenue_d{day}'] / m1),
                                      (len(self.time_columns['net_revenue']), 1)).T *
                              np.tile(mean_all, (len(self.df_full), 1))).set_index(self.df_full.index)

        df.columns = self._net_rev_columns
        return df

    def get_difference_frame(self):
        df = (self.df_revenues.T - self.df_revenues.T.shift(1)).T
        df.columns = [f'diff_to_day_d{d}' for d in self.time_columns['net_revenue']]
        df = df.drop('diff_to_day_d0', axis=1)
        return df.where(df > 0, np.nan)

    def get_optional_columns(self, day=None):
        if day is None:
            return sum([[f'{col}_d{t}' for t in self.time_columns[col]] for col in self._optional_columns], [])
        else:
            return sum([[f'{col}_d{t}' for t in self.time_columns[col] if int(t) <= day] for col in self._optional_columns], [])


class UserCommitment:
    def __init__(self, torso):
        self._column_name = torso
        self._publisher = None
        self._join_columns = ['date', 'level1', 'level2']
        self._diff_columns = []
        self._df = None
        self._diff_columns_enabled = None

    @classmethod
    def init_by_file(cls, column_name, file_path, publisher, last_day=29, **kwargs):
        obj = cls(column_name)
        obj.build_day_frame(file_path, publisher, last_day, **kwargs)
        return obj

    def build_day_frame(self, file_path, publisher, last_day, **kwargs):
        try:
            df = pd.read_csv(file_path, **kwargs).sort_values(['publisher', 'level1', 'level2', 'date', 'days_since'])
        except FileNotFoundError:
            raise FileNotFoundError(f'{file_path} is inexistent')

        self._df = pd.concat([df[(df.publisher == publisher) & (df.days_since == day)].set_index(
            self._join_columns)[self._column_name].rename(f'{self._column_name}_{day}')
                         for day in np.arange(1, last_day)], axis=1)

        # raise RuntimeError
        if last_day > 1:
            self._build_differences(last_day)

    def _build_differences(self, last_day):
        for day in np.arange(last_day-1, 1, -1):
            self._df.loc[:, f'{self._column_name}_diff_{day-1}_{day}'] = \
                self._df[f'{self._column_name}_{day}'] - self._df[f'{self._column_name}_{day-1}']
            self._diff_columns.append(f'{self._column_name}_diff_{day-1}_{day}')

    def join_to_day_frame(self, df_day, day):
        if day == 1:
            df_return = df_day.join(self._df[[f'{self._column_name}_{d}'for d in np.arange(1, day+1)]], on=self._join_columns)
            return df_return, [f'{self._column_name}_{d}'for d in np.arange(1, day)]
        elif self._diff_columns_enabled:
            diff_column_list = [c for c in self._diff_columns if int(c.strip().split('_')[-1]) <= day]
            df_return = df_day.join(self._df[[f'{self._column_name}_{d}'for d in np.arange(1, day+1)] + diff_column_list], on=self._join_columns)
            return df_return, [f'{self._column_name}_{d}'for d in np.arange(1, day)] + diff_column_list
        else:
            print([f'{self._column_name}_{d}'for d in np.arange(1, day+1)])
            df_return = df_day.join(self._df[[f'{self._column_name}_{d}'for d in np.arange(1, day+1)]], on=self._join_columns)
            return df_return, [f'{self._column_name}_{d}'for d in np.arange(1, day+1)]

    def disable_diff_columns(self):
        self._diff_columns_enabled = False

    def rename_columns(self, new_torso):
        self._df.columns = [c.replace(self._column_name, new_torso) for c in self._df]
        self._column_name = new_torso

    def __truediv__(self, rs):
        if (self._publisher != rs._publisher) or (self._join_columns != rs._join_columns):
            raise RuntimeError('Left- and Righthandside are too different')
        #print(self._df.index.names, rs._df.index.names)
        #print(self._df.shape, rs._df.shape)
        #assert_index_equal(self._df.index, rs._df.index)

        obj = UserCommitment(f'{self._column_name}_by_{rs._column_name}')
        obj._publisher = self._publisher
        obj._join_columns = self._join_columns

        re_left, re_right = re.compile(f'{self._column_name}_\d+$'.replace('_', '\_')), \
            re.compile(f'{rs._column_name}_\d+$'.replace('_', '\_'))
        dfL = self._df.copy()[[c for c in self._df.columns if re_left.match(c)]]
        dfR = rs._df.copy()[[c for c in rs._df.columns if re_right.match(c)]]
        dfL.columns = [obj._column_name + '_' + c.strip().split('_')[-1] for c in dfL.columns.values]
        dfR.columns = [obj._column_name + '_' + c.strip().split('_')[-1] for c in dfR.columns.values]
        obj._df = dfL.div(dfR)

        return obj

    def __sub__(self, rs):
        if (self._publisher != rs._publisher) or (self._join_columns != rs._join_columns):
            raise RuntimeError('Left- and Righthandside are too different')
        assert_index_equal(self._df.index, rs._df.index)

        obj = UserCommitment(f'{self._column_name}_minus_{rs._column_name}')
        obj._publisher = self._publisher
        obj._join_columns = self._join_columns

        re_left, re_right = re.compile(f'{self._column_name}_\d+$'.replace('_', '\_')), \
            re.compile(f'{rs._column_name}_\d+$'.replace('_', '\_'))
        dfL = self._df.copy()[[c for c in self._df.columns if re_left.match(c)]]
        dfR = rs._df.copy()[[c for c in rs._df.columns if re_right.match(c)]]
        dfL.columns = [obj._column_name + '_' + c.strip().split('_')[-1] for c in dfL.columns.values]
        dfR.columns = [obj._column_name + '_' + c.strip().split('_')[-1] for c in dfR.columns.values]
        obj._df = dfL - dfR

        return obj


class InitialProperty:
    def __init__(self, column_name):
        self._ps = None
        self._publisher = None
        self._column_name = column_name
        self._join_columns = ['date', 'level1', 'level2']

    @classmethod
    def init_by_file(cls, column_name, file_path, publisher, **kwargs):
        obj = cls(column_name)        
        obj.build_series(file_path, publisher, **kwargs)
        return obj

    @classmethod
    def init_by_commitment():
        pass

    def build_series(self, file_path, publisher, **kwargs):
        try:
            df = pd.read_csv(file_path, **kwargs)
        except FileNotFoundError:
            raise FileNotFoundError(f'{file_path} is inexistent')

        self._publisher = publisher
        self._ps = df[(df.publisher == publisher) & (df.days_since == 1)].set_index(self._join_columns)[
            self._column_name]

    def transform(self, func, *args, **kwargs):
        idx = self._ps.index
        self._ps = pd.Series(func(self._ps, *args, **kwargs), idx, name=self._ps.name)

    def join_to_day_frame(self, df_day, *args, **kwargs):
        df_return = df_day.join(self._ps, on=self._join_columns)
        return df_return, [self._column_name]

    def __truediv__(self, rs):
        if self._publisher != rs._publisher:
            raise RuntimeError('Left- and Righthandside are too different')
        assert_index_equal(self._ps.index, rs._ps.index)

        obj = InitialProperty(f'{self._column_name}_by_{rs._column_name}')
        obj._ps = (self._ps / rs._ps)
        obj._ps = obj._ps.rename(f'{self._column_name}_by_{rs._column_name}')

        obj._publisher = self._publisher

        return obj


class ModelOfDay:
    def __init__(self, day, end_day=29, residual_target=False):
        self._df = None
        self._bins = 40
        self._day = day
        self._trainings_days = []
        self._bygone_days = []
        self._commit_columns = []
        self._end_day = end_day
        self._regressors = {}
        self._fit_parameters = {}
        self._residual_target = residual_target
        self._general_trainings_columns = ['spend', 'clicks', 'installs', 'impressions']
        self._daybased_trainings_columns = []

    def __getitem__(self, key):
        return self._regressors[key]

    @property
    def df(self):
        return self._df

    def merge_commitment(self, commitment):
        df_commitment, commit_columns = commitment.join_to_day_frame(self._df, self._day)
        self._df = pd.concat([self._df, df_commitment[commit_columns]], axis=1)
        self._daybased_trainings_columns += commit_columns

    def merge_initial_property(self, initial_property):
        pass

    def build_trainings_data(self, dm):
        self._df = dm.df_full.copy().set_index(dm.df_raw.index)
        self._trainings_days = [d for d in dm.time_columns['net_revenue'] if d > self._day and d <= self._end_day]
        self._bygone_days = [d for d in dm.time_columns['net_revenue'] if d <= self._day]
        self._df = pd.concat([self._df, dm.get_difference_frame()], axis=1)

        if self._day == 1:
            self._daybased_trainings_columns.append('net_revenue_d1')
        elif self._day == 2:
            #self._df.loc[:, 'relative_rise'] = ((self._df[f'diff_to_day_d{self._day}'] -
            #                                     self._df['diff_to_day_d1']) / self._df['diff_to_day_d1']).fillna(0)
            self._daybased_trainings_columns += ['diff_to_day_d2', 'net_revenue_d1']
        else:
            #self._df.loc[:, 'relative_rise'] = ((self._df[f'diff_to_day_d{self._day}'] -
            #                                     self._df['diff_to_day_d1']) / self._df['diff_to_day_d1']).fillna(0)
            #self._df.loc[:, 'relative_rise_to_2'] = ((self._df[f'diff_to_day_d{self._day}'] -
            #                                          self._df['diff_to_day_d2']) / (self._df[f'diff_to_day_d2'] -
            #                                                                         self._df['diff_to_day_d1'])).fillna(0)
            #self._daybased_trainings_columns += [f'net_revenue_d{d}' for d in np.arange(1, self._day + 1)] + [f'diff_to_day_d{d}' for d in np.arange(1, self._day + 1)]
            self._daybased_trainings_columns += [f'diff_to_day_d{d}' for d in np.arange(1, self._day + 1)]
        self._daybased_trainings_columns += dm.get_optional_columns(self._day)

    def _add_dreisatz_residuals(self, dm, idx_train):
        # auch test mit durchschleifen?
        df3s = dm.get_dreisatz(self._day, idx_train)
        df_residual = self.df.loc[idx_train][[f'net_revenue_d{d}' for d in self._trainings_days]] - \
            df3s[[f'net_revenue_d{d}' for d in self._trainings_days]]
        df_residual.columns = [f'residual_d{d}' for d in self._trainings_days]
        return pd.concat([self.df.loc[idx_train], df_residual], axis=1)

    def build_feature_list(self, df_train):
        return FeatureList([Feature(var, VariableFlag.CONTINUUOUS, meta={'bins': self._bins}) for var in
                            self._general_trainings_columns + self._daybased_trainings_columns if not self._without_information(df_train, var)]
                           + [Feature(var, VariableFlag.CATEGORICAL) for var in ['weekday', 'iOS', 'Android']])

    def train_single_sample(self, dm, idx_train=None, classifier_file=None, feature_list=None, classifier='random_forest'):
        df_train = self._add_dreisatz_residuals(dm, idx_train)
        classifier_file_ = '/home/johannes/ITM/lighthouse/ltv/classifiers7.yaml' if not classifier_file else classifier_file

        features_ = FeatureList([Feature(var, VariableFlag.CONTINUUOUS, meta={'bins': self._bins}) for var in
                                 self._general_trainings_columns + self._daybased_trainings_columns if not self._without_information(df_train, var)]
                                + [Feature(var, VariableFlag.CATEGORICAL) for var in ['weekday', 'iOS', 'Android']])
        if feature_list is not None:
            features = [f for f in features_ if f.name in feature_list]
        else:
            features = features_

        for day in self._trainings_days:
            self._regressors[day] = OpdapRegressor(features, classifier=classifier, classifier_file=classifier_file_)
            if self._residual_target:
                self._regressors[day].fit(df_train, f'residual_d{day}', Targetbins=20)
            else:
                self._regressors[day].fit(df_train, f'net_revenue_d{day}', Targetbins=40)

    def predict_single_sample(self, dm, idx_train, idx_test=None):
        df3s = dm.get_dreisatz(self._day, idx_train, idx_test)[[f'net_revenue_d{day}' for day in self._trainings_days]]
        #df3s.columns = [f'dreisatz_d{d}' for d in self._trainings_days]
        df_test = self.df.copy() if idx_test is None else self.df.loc[idx_test]
        test_preds = {}
        df_test = pd.concat([df_test.loc[idx_test], df3s], axis=1)
        for day in self._trainings_days:
            # hier test beachten!
            try:
                if self._residual_target:
                    test_preds[day] = self._regressors[day].predict(df_test) + df3s[f'net_revenue_d{day}'].values
                else:
                    test_preds[day] = self._regressors[day].predict(df_test)
            except ValueError:
                print(f'VALUE ERROR RAISED ON DAY {day}')
                raise ValueError
        df_ret = pd.DataFrame(test_preds).set_index(df_test.index)
        for d in self._bygone_days:
            df_ret = df_ret.join(df_test[f'net_revenue_d{d}'].rename(d))
        return df_ret[sorted(df_ret.columns)]

    def fit_function(self, df):
        # oder anders, nur eine Zeile?
        self._fit_parameters = {}
        for idx, values in df.iterrows():
            try:
                p_pred, _ = curve_fit(cdf_gamma, df.columns,
                                      values,
                                      p0=[5, 2, 2], maxfev=2000)
                self._fit_parameters[idx] = p_pred
            except RuntimeError:
                pass

    def obtain_prediction(self, idx_predict=None):
        idxs = self.df.index.values if idx_predict is None else idx_predict
        prediction = {}
        for idx, values in self.df.loc[idxs].iterrows():
            try:
                prediction[idx] = cdf_gamma(self._trainings_days, *self._fit_parameters[idx])
            except KeyError:
                print('KEYERROR', idx)
                continue
        df = pd.DataFrame(prediction).T
        df.columns = [f'net_revenue_d{d}' for d in self._trainings_days]
        return df

    def _without_information(self, df, var):
        if df[var].nunique() == 1:
            return True
        else:
            return False
