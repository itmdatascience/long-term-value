#!/usr/bin/env python
from setuptools import setup, find_packages


setup(name='ltv',
      author='Johannes Gygier',
      python_requires='>=3.7',
      install_requires=['numpy>=1.14.5', 'sqlalchemy', 'pandas>=0.21.0', 'PyYAML>=3.12', 'scipy>=1.5', 'opdap'],
      packages=find_packages())
